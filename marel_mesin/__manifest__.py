{
    "name"          : "Marel Kerusakan dan Mesin",
    "version"       : "1.0",
    "author"        : "Miftahussalam",
    "website"       : "http://miftahussalam.com",
    "category"      : "New Module",
    "summary"       : "Belajar Membuat Addons",
    "description"   : """

    """,
    "depends"       : [
        "base",
    ],
    "data"          : [
        "views/menu.xml",
        "views/daftar_kerusakan_mesin.xml",
        "views/marel_jenis_kerusakan_mesin.xml",
        "views/marel_mesin_produksi.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}