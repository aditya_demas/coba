#  ------------------ mesin
class shuttle(models.Model):
    	_name = 'mesin.shuttle'
	_description = "shuttle"
	_rec_name = 'name'

	name = fields.Selection([
		("52",_("52")),
		("56",_("56")),
		("75",_("75"))], string='Mesin', required=True, )
	start_date = fields.Date(string='Tgl Dari',default=fields.Date.context_today,)
	kode_kain = fields.Many2one(string="Produk", comodel_name= "product.template", ondelete="set null", )
	divisi = fields.Char(string="DIVISI", default="SHUTTLE")
	rusak = fields.One2many("kerusakan.shuttle1", "kerusakanshuttle_ids", "kerusakan_mesin_shuttle1",)

# ------------------ data kerusakan
class kerusakan_shuttle(models.Model):
	_name = "kerusakan.shuttle"
	_description = "kerusakan_mesin_shuttle"
	_rec_name = "kerusakanshuttle"

	kerusakanshuttle = fields.Char(string="Kerusakan Shuttle", required=True,)

# -------------------- list kerusakan mesin
class kerusakan_shuttle1(models.Model):
	_name = "kerusakan.shuttle1"
	_description = "kerusakan_mesin_shuttle1"
	_rec_name = "kerusakanshuttle_ids"

	rusak = fields.Many2one("kerusakan.shuttle", "kerusakanshuttle")
	kerusakanshuttle_ids = fields.Many2one("mesin.shuttle", "shuttle")
	no_ids= fields.Many2one(string="No Mesin", comodel_name="mesin.produksi", ondelete="set null", )
	block = fields.Char(string="Block", required=False,)
	deret = fields.Char(string="Deret", required=False,)
	rpm = fields.Integer(string='RPM')
	beam = fields.Many2one(string="No Beam",comodel_name="nomor.beam", ondelete="set null", )
	jenis_mesin = fields.Many2one(string="Jenis Mesin", comodel_name="mesin.shuttle", ondelete="set null",) 