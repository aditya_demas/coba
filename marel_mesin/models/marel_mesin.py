from flectra import api, fields, models, _

class MarelMesinProduksi(models.TransientModel):
	_name = 'marel.mesin.produksi'
	_rec_name = 'machine'

	machine= fields.Char(string="Machine")
	#divisi = fields.Many2one(string="DIVISI", comodel_name="divisi.divisi", ondelete="set null")
	modelloan = fields.Char(string="Modelloan", required=False, )
	annofabbr = fields.Char(string="Annofabbr", required=False,)
	matricolan = fields.Char(string="Matricolan", required=False,)
	needle = fields.Integer(string='Needle')
	finezza_aghi = fields.Integer(string='Finezza Aghi')
	diametro = fields.Char(string='Diametro')

#-------------- tanpa view --------------------
class MarelJenisKerusakanMesinList(models.TransientModel):
	_name = 'mareljenis.kerusakanmesin.list'
	_rec_name = 'jenis_kerusakan_mesin_list'

	jenis_kerusakan_mesin_list = fields.Many2one('marel.jenis.kerusakanmesin', 'Data Kerusakan di Mesin',)
	daftar_kerusakan_mesin_id = fields.Many2one('daftar.kerusakan.mesin', 'List Kerusakan')

# --------------------------------------------------------

class MarelJenisKerusakanMesin(models.TransientModel):
	_name = 'marel.jenis.kerusakanmesin'
	_rec_name = 'jenis_kerusakan_mesin'

	jenis_kerusakan_mesin = fields.Char(string="Jenis Keruskan Mesin", reqired = True,)


class DaftarKerusakanMesin(models.TransientModel):
	_name = 'daftar.kerusakan.mesin'
	_rec_name = 'machine'

	status = fields.Selection([
		('Error',_('Error')),
		('Udah_benar',_('Udah_Benar'))],string="Status",)	
	machine = fields.Many2one('marel.mesin.produksi',string="Machine", required=True,)
	jenis_kerusakan_mesin_id = fields.One2many('mareljenis.kerusakanmesin.list','daftar_kerusakan_mesin_id','Data Kerusakan Mesin')



