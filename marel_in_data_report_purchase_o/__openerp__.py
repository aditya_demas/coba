{
	"name": "Report PO",
	"version": "1.0", 
	"depends": [
		"base",
		"purchase",
	],
	"author": "asrent345@gmail.com", 
	"category": "Education", 
	'website': 'http://www.vitraining.com',
	"description": """\

Manage
======================================================================

* this is my academic information system module
* created menu:
* created object
* created views
* logic:

""",
	"data": [
		"view/purchasse.xml",
		"report/marel_in_data_report_purchase_o.xml",
		#-------------------- coba PO
		"report/marel_report_po.xml",
		#"report/marel_report_po_aksesoris.xml",
		#"report/marel_report_po_barcode.xml",
		#"report/marel_report_po_benang.xml",
		#"report/marel_report_po_hangtag.xml",
		#"report/marel_report_po_sole.xml",
		#"report/marel_report_po_spare_parts_import.xml",
		#"report/marel_report_po_spare_parts_lokal.xml",
		#-------------------------- report
		"report/report_marel_report_po_pemesanan_aksesoris_doc.xml",
		"report/report_marel_report_po_pemesanan_barcode_doc.xml",
		"report/report_marel_report_po_pemesanan_benang_doc.xml",
		"report/report_marel_report_po_pemesanan_hangtag_doc.xml",
		"report/report_marel_report_po_pemesanan_sole_doc.xml",
		"report/report_marel_report_po_pemesanan_spare_parts_import_doc.xml",
		"report/report_marel_report_po_pemesanan_spare_parts_lokal_doc.xml",
		"report/report_marel_report_po_konfirmasi_pesanan_doc.xml",		
		"report/report_marel_report_po_sale_order_produksi_doc.xml",
		# draff--------------------------------------------
		"report/report_marel_report_po_pemesanan_aksesoris_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_barcode_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_benang_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_hangtag_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_sole_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_spare_parts_import_draff_doc.xml",
		"report/report_marel_report_po_pemesanan_spare_parts_lokal_draff_doc..xml",
	],
	"installable": True,
	"auto_install": False,
    "application": True,
}