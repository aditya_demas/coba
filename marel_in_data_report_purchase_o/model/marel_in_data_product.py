from flectra import api, fields, models, _
from datetime import datetime

class InMrpProductTemplate(models.Model):
    #_name = 'product.template'
    _inherit = 'product.template'    

    #product
    nama_desain = fields.Char(string='Nama Desain',)
    no_mesin= fields.Float(string='No Mesin/Jarum',)
    berat= fields.Float(string='Berat/Pasang',)
    warna = fields.Char(string='Warna',)
    tgl_buat = fields.Date(string='tgl Buat',default=fields.Date.context_today,readonly=True,)

    gum_stretch_x= fields.Float(string='Gum Stretch x ',)
    gum_stretch_y= fields.Float(string='Gum Stretch y ',)
    leg_gum_stretch_x= fields.Float(string='Leg Gum Stretch x ',)
    leg_gum_stretch_y= fields.Float(string='Leg Gum Stretch y ',)
    leg_stretch_x= fields.Float(string=' Leg Stretch x',)
    leg_stretch_y= fields.Float(string='Leg Stretch y ',)
    foot_stretch_x= fields.Float(string=' Foot Stretch x',)
    foot_stretch_y= fields.Float(string=' Foot Stretch y',)
    foot_gum_stretch_x= fields.Float(string='Foot Gum Stretch x ',)
    foot_gum_stretch_y= fields.Float(string='Foot Gum Stretch y ',)
    hell_stretch_x= fields.Float(string='Hell Stretch x ',)
    hell_stretch_y= fields.Float(string='Hell Stretch y ',)

    gum_relaxed_x= fields.Float(string='Gum Relaxed x ',)
    gum_relaxed_y= fields.Float(string='Gum Relaxed y ',)
    leg_gum_relaxed_x= fields.Float(string='Leg Gum Relaxed x ',)
    leg_gum_relaxed_y= fields.Float(string='Leg Gum Relaxed y ',)
    leg_relaxed_x= fields.Float(string=' Leg Relaxed x',)
    leg_relaxed_y= fields.Float(string='Leg Relaxed y ',)
    foot_relaxed_x= fields.Float(string=' Foot Relaxed x',)
    foot_relaxed_y= fields.Float(string=' Foot Relaxed y',)
    foot_gum_relaxed_x= fields.Float(string='Foot Gum Relaxed x ',)
    foot_gum_relaxed_y= fields.Float(string='Foot Gum Relaxed y ',)
    hell_relaxed_x= fields.Float(string='Hell Relaxed x ',)
    hell_relaxed_y= fields.Float(string='Hell Relaxed y ',)

    #bahan baku    
    kode_benang = fields.Char(string='Kode Benang',)
    no_warna_benang = fields.Char(string='No Warna Benang',)
    warna_benang = fields.Char(string='Warna Benang',)
    jenis_benang = fields.Char(string='Jenis Benang',)


"""


#--------------------------------- class baru -----------------------------------------------
class InPlanMes(models.TransientModel):
    _description = "Plan MES PPIC"
    _name = "in.plan.mes"
    #_rec_name = "no_mo"

    kode_plan_mesin = fields.Char(string='Kode Plan Mesin',required=True)
    no_kp = fields.Many2one('mrp.production',string='No KP/MO')
    nama_template = fields.Many2one(related='no_kp.product_tmpl_id',string='Nama Barang',readonly=True,)
    date_start = fields.Date(string="Start Date", required=True)
    date_end = fields.Date(string="End Date", required=True)

    real_date_start = fields.Date(string="Real Start Date",)
    real_date_end = fields.Date(string="Real End Date",)


#--------------------------------- class baru -----------------------------------------------
class InHeaderPlanWarping(models.TransientModel):
    _description = "Header Plan Warping"
    _name = "inherader.planwarping"
    _rec_name = "name"


    kode_warping = fields.Char(string='Kode Plan Warping',required=True)
    name = fields.Many2one('mrp.production',string='No KP/MO')
    seri = fields.Char(string='Seri',)
    tarikan = fields.Char(string='Tarikan',)
    quantity = fields.Float(string='Quantity',)
    mesin = fields.Many2one('mesin.produksi',string='Mesin Warping')
    date_start = fields.Date(string="Start Date", required=True)
    date_end = fields.Date(string="End Date", required=True)


#--------------------------------- class baru -----------------------------------------------
class InOperatorMengisiWarping(models.TransientModel):
    _description = "Operator Mengisi Warping"
    _name = "inoperator.mengisiwarping"
    _rec_name = "name"

    no_kp = fields.Many2one('inherader.planwarping',string='No KP/MO')
    seri = fields.Char(related='no_kp.seri',string='Seri',readonly=True,)
    tarikan = fields.Char(related='no_kp.tarikan',string='Tarikan',readonly=True,)
    quantity = fields.Float(related='no_kp.quantity',string='Quantity',readonly=True,)
    mesin = fields.Many2one(related='no_kp.mesin',string='Mesin Warping', readonly=True,)
    date_start = fields.Date(related='no_kp.date_start',string="Start Date",readonly=True,)
    date_end = fields.Date(related='no_kp.date_end',string="End Date",readonly=True,)

    #jumlah_benang_ppic = fields.Integer(string='Jumlah Benang PPIC', )

    #-------------------------------- operator mngisi -------------------------------------
    name = fields.Char(string='Kode Plan Warping',required=True)

    lebar_kain = fields.Integer(string='Lebar Kain', )
    
    lusi_pakan = fields.Char(string='Lusi/Pakan', )
    
    jumlah_benang_lusi = fields.Integer(string='Jumlah Benang Lusi/Pakan', )
    
    kk_benang_rata = fields.Integer(string='Kk Benang Rata2', )
    
    kl_benang_rata = fields.Float(string='Kl Benang Rata2', )
    
    pts_benang_rata = fields.Integer(string='Pts Benang Rata2', )

    ne_benang_rata = fields.Char(string='NE Benang Rata2', )

    #mulai dr urut k kanan

    jmlh_benang = fields.Integer(string='Jmlh Benang', ) #

    panjang_benang = fields.Integer(string='panjang benang', )

    no_beam = fields.Char(string='No Beam', ) #op

    pts_benang = fields.Integer(string='Pts Benang', ) #op

    pts_persejuta = fields.Float(string='pts/1juta', ) #op

    berat_bruto = fields.Float(string='Berat Bruto', )

    berat_beam = fields.Float(string='Berat Beam', )

    berat_neto = fields.Float(string='Berat Neto', )
    
    #ne_bng = fields.Float(string='NE Bng', )
    
    tgl = fields.Date(string='Tgl', required=True)
    
    jam_naik = fields.Char(string='Jam Naik')
    
    jam_turun = fields.Char(string='Jam Turun',)

    lama_proses_menit = fields.Integer(string='Lama Proses Menit',)

    rata_menit = fields.Float(string='Rata2 perMenit',)

    operator = fields.Char(string='Operator',)
    
    sebelum_dikanji_gram = fields.Char(string='Sebelum Dikanji Gram',)

    sebelum_dikanji_persen = fields.Char(string='Sebelum Dikanji %',)
    
    hns_kanan = fields.Char(string='HNS Kanan',)

    hns_tengah = fields.Char(string='HNS Tengah',)
    
    hns_kiri = fields.Char(string='HNS Kiri',)

    kontrol_benang = fields.Boolean(String='Kontrol Benang',)


#--------------------------------- class baru -----------------------------------------------
class InLaporanHarianShiftWarping(models.TransientModel):
    _description = "Laporan Harian Shift Warping"
    _name = "in.laporanharian.shiftwarping"
    #_rec_name = "name"

    no_kp = fields.Many2one('inherader.planwarping',string='No KP/MO')
    seri = fields.Char(related='no_kp.seri',string='Seri',readonly=True,)
    tarikan = fields.Char(related='no_kp.tarikan',string='Tarikan',readonly=True,)
    quantity = fields.Float(related='no_kp.quantity',string='Quantity',readonly=True,)
    mesin = fields.Many2one(related='no_kp.mesin',string='Mesin Warping', readonly=True,)
    date_start = fields.Date(related='no_kp.date_start',string="Start Date",readonly=True,)
    date_end = fields.Date(related='no_kp.date_end',string="End Date",readonly=True,)

#------------------------- operator ------------------------------------------------------
    tgl = fields.Date(string='Tgl', required=True)

    operator = fields.Char(string='Operator',)

    waktu = fields.Char(string='waktu/jam',)

    panjang_tiap_beam = fields.Char(string='Panjang Tiap Beam', )

    hasil_perbeam = fields.Char(string='Hasil  Per Beam', )


    pts_persejuta = fields.Float(string='pts/1juta', )

    provit = fields.Integer(string='Provit', )

    ttl_lost_time = fields.Char(string='TTL Lost Time', )

    eff = fields.Char(string='EFF', )
    
    keterangan = fields.Text(tring='Keterangan',)


#--------------------------------- class baru -----------------------------------------------
class InHeaderPlanSizing(models.TransientModel):
    _description = "Header Plan Sizing"
    _name = "inherader.plansizing"
    _rec_name = "name"


    mo_warping = fields.Many2one('inherader.planwarping',string='MO Warping',)
    kode_warping = fields.Char(related='mo_warping.kode_warping',string='Kode Warping',readonly=True,)
    kode_sizing = fields.Char(string='Kode Sizing',)
    name = fields.Many2one('mrp.production',string='No KP/MO')
    seri = fields.Char(string='Seri',)
    tarikan = fields.Char(string='Tarikan',)
    quantity = fields.Float(string='Quantity',)
    mesin = fields.Many2one('mesin.produksi',string='Mesin Warping')
    date_start = fields.Date(string="Start Date", required=True)
    date_end = fields.Date(string="End Date", required=True)


#--------------------------------- class baru -----------------------------------------------
class InOperatorSizing_Depan(models.TransientModel):
    _description = "Operator Sizing Depan"
    _name = "in.operator.sizingdepan"


    #ubah fields
    name = fields.Many2one('inherader.plansizing',string='No KP/MO')
    kode_sizing = fields.Char(related='name.kode_sizing',string='Seri',readonly=True,)
    seri = fields.Char(related='name.seri',string='Seri',readonly=True,)
    tarikan = fields.Char(related='name.tarikan',string='Tarikan',readonly=True,)
    quantity = fields.Float(related='name.quantity',string='Quantity',readonly=True,)
    mesin = fields.Many2one(related='name.mesin',string='Mesin Warping', readonly=True,)
    date_start = fields.Date(related='name.date_start',string="Start Date",readonly=True,)
    date_end = fields.Date(related='name.date_end',string="End Date",readonly=True,)

    tgl = fields.Date(string='Tgl', required=True)
    grade = fields.Char(string='Grade',)
    seri = fields.Char(string='Seri',)
    #------------------ beda ------------------------------------
    warping = fields.Char(string='Warping')
    mc_szg = fields.Many2one(string="MC Szg",comodel_name="mesin.produksi",ondelete='set null',)
    mc_wvp = fields.Many2one(string="MC WVP",comodel_name="mesin.produksi",ondelete='set null',)
    #---------------------------------- operator
    asal_benang = fields.Char(string='Asal Benang', )
    elg = fields.Integer(string='ELG', )
    stg = fields.Integer(string='STG', )
    pts_benang = fields.Char(string='Pts Benang', )
    te = fields.Char(string='TE', )
    pjg = fields.Integer(string='Pjg', )
    
    no_beam = fields.Char(string='no Beam', )
    panjang_benang = fields.Integer(string='Panjang Benang', )
    bruto = fields.Float(string='Berat Bruto', )
    beam = fields.Float(string='Berat Beam', )
    netto = fields.Float(string='Berat Netto', )
    spu = fields.Float(string='SPU', )
    visco_1 = fields.Integer(string='Visco 1', )
    visco_2 = fields.Integer(string='Visco 2', )
    suhu_1 = fields.Integer(string='Suhu 1', )
    suhu_2 = fields.Integer(string='Suhu 2', )
    ref = fields.Char(string='REF', )    
    jam_naik = fields.Char(string='Jam Naik', )
    jam_turun = fields.Char(string='Jam Turun', )
    hasil_menit = fields.Char(string='Hasil Menit', )
    #tambahan
    hasil_shift = fields.Char(string='Hasil Shift', )
    speed = fields.Integer(string='Speed', )
    press_dp_blk = fields.Char(string='Press Dp Blk', )
    temp_cyl_1 = fields.Char(string='Temp Cyl 1', )
    temp_cyl_2 = fields.Char(string='Temp Cyl 2', )
    temp_cyl_3 = fields.Char(string='Temp cyl 3', )
    operator_db_blk = fields.Char(string='Operator Db Blk', )
    #tambahan
    leader = fields.Integer(string='Leader', )
    leadery = fields.Integer(string='Putus Benang Creel', )
    putus_benang_size_box = fields.Integer(string='Putus Benang Size Box', )
    putus_benang_sisir = fields.Integer(string='Putus Benang Sisir', )
    
    #----------------------------------------------------------------------------------------
        #era bawah
    informasi = fields.Text(string='Informasi', )
    keterangan = fields.Text(string='Keterangan', )
    
    afal_setting_m = fields.Integer(string='Afal Setting M', )
    afal_setting_kg = fields.Float(string='Afal Setting KG', )

    alfa_terkaji_awal = fields.Float(string='Alfa Terkaji Awal KG', )
    alfa_terkaji_akhir = fields.Float(string='Alfa Terkaji Akhir KG', )
    alfa_terkaji_total = fields.Float(string='Alfa Terkaji Total KG', )    
    
    alfa_belum_awal = fields.Float(string='Alfa Belum Awal KG', )
    alfa_belum_akhir = fields.Float(string='Alfa Belum Akhir KG', )
    alfa_belum_total = fields.Float(string='Alfa Belum Total KG', )

    pesiapan_size_box_finish = fields.Float(string='Pesiapan Size Box Finish', )
    pesiapan_size_box_start = fields.Float(string='Pesiapan Size Box Start', )
    pesiapan_size_box_total = fields.Float(string='Pesiapan Size Box Total', )

    #--------------------------------------------------------------------------------------------
    pesiapan_cyl_finish = fields.Float(string='Pesiapan Cyl Finish', )
    pesiapan_cyl_start = fields.Float(string='Pesiapan Cyl Start', )
    pesiapan_cyl_total = fields.Float(string='Pesiapan Cyl Total', )

    pesiapan_head_finish = fields.Float(string='Pesiapan Head Finish', )
    pesiapan_head_start = fields.Float(string='Pesiapan Head Start', )
    pesiapan_head_total = fields.Float(string='Pesiapan Head Total', )

    pesiapan_pesankan_finish = fields.Float(string='Pesiapan Pesankan Finish', )
    pesiapan_pesankan_start = fields.Float(string='Pesiapan Pesankan Start', )
    pesiapan_pesankan_total = fields.Float(string='Pesiapan Pesankan Total', )
    

# ----------------------------------------  beda class ----------------------------------------
class InSerahTrimabenang(models.TransientModel):
    _description = "Serah Trimabenang"
    _name = "in.serah.trimabenang"
    

    tgl_buat = fields.Date(string='Tanggal Buat',default=fields.Date.context_today,readonly=True,)
    #waktu_create =  fields.Date(string='Waktu Create', default=datetime.today()readonly=True,)
    no_kp = fields.Many2one('mrp.production',string='No KP/MO')
    kode_benang = fields.Char(related='no_kp.product_tmpl_id.kode_benang',string='Kode Benang',readonly=True,)
    jenis_benang = fields.Char(related='no_kp.product_tmpl_id.jenis_benang',string='Jenis Benang',readonly=True,)
    #jenis_benang = fields.Char(related='no_kp.product_tmpl_id.jenis_benang',string='Jenis Benang',readonly=True,)
    #kode_benang = fields.Char(related='no_kp.product_tmpl_id.kode_benang',string='Kode Benang',readonly=True,)
    
    jumlah_bon = fields.Float(string='Jumlah Bon',)
    
    #kg = fields.Float(string='KG', )
    
"""