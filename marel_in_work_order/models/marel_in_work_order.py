
# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from flectra import models, fields, api, _
from flectra.exceptions import UserError, ValidationError


class MarelInWorkOrder(models.Model):
    # _name = 'mrp.workorder'    
    _inherit = ['mrp.workorder']
    jumlah_yg_dikurangi = fields.Integer(string='Jumlah Yg Dikurangi',)
    
    
    # _name = 'ModelName'
    # _description = u'ModelName'

    # _rec_name = 'name'
    # _order = 'name ASC'

    @api.onchange(
        'kurangi_jumlah_kkp',
        )
    def get_kurangi_jumlah_kkp(self):
        self.qty_producing = 0
        kurang_kkp = self.qty_producing + self.jumlah_yg_dikurangi
        self.qty_producing = kurang_kkp
        return