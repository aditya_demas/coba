from flectra import api, fields, models, _
from flectra.exceptions import UserError


class InMrpProductTTemplate2(models.TransientModel):
    _inherit = "product.template"
    _name = 'product.template'
    #_inherit = 'product.template'    
    #_rec_name = 'kode'

    
    #nama_product = fields.Many2one(string='Nama Product',comodel_name='product.template', ondelete='set null',)
    deskripsi = fields.Text("Deskripsi")
    kode_benang = fields.Char(string='Kode Benang',)
    jenis_benang = fields.Char(string='Jenis Benang',)
    kelompok = fields.Selection([
        ('BENANG',_('BENANG')),
        ('BENANGWARNA',_('BENANGWARNA')),
	    ('BENANGWO',_('BENANGWO')),
	    ('CHEMICAL',_('CHEMICAL')),
	    ('FUEL',_('FUEL')),
	    ('GREIGE',_('GREIGE')),
        ('OIL',_('OIL')),
        ('SERVICE',_('SERVICE')),
        ('SIZING',_('SIZING')),
        ('SPAREPART',_('SPAREPART')),
        ('SPAREPARTRAPIER',_('SPAREPARTRAPIER')),
        ('SPAREPARTSHUTTLE',_('SPAREPARTSHUTTLE')),
	    ('UTILITY',_('UTILITY'))], string="Kelompok",)
    jenis_1 = fields.Char("Jenis 1")
    jenis_2 = fields.Char("Jenis 2")
    lebar_ne = fields.Float(string='Lebar/NE',)
    lusi = fields.Float(string='Lusi',)
    pakan = fields.Float(string='pakan',)
    konstruksi_1 = fields.Char(string='Konstruksi 1',)
    konstruksi_2 = fields.Char(string='Konstruksi 2',)
    lse = fields.Float(string='LSE',)
    helai_lusi = fields.Float(string='Helai Lusi',)
    helai_pakan = fields.Float(string='Helai Pakan',)
    shrinkage = fields.Float(string='Shrinkage',)
    sisir = fields.Float(string='Sisir',)
    endsisir = fields.Float(string='Endsisir',)
    kamran = fields.Float(string='Kamran',)
    cf = fields.Float(string='CF',)
    kapasitas = fields.Float(string='Kapasitas',)
    target = fields.Float(string='Target',)
    tipe = fields.Char(string='Type',)
    supplier = fields.Char(string='Supplier',)
    customer = fields.Char(string='Customer',)
    #nama = fields.Char(string='Nama',)
    #code = fields.Char(string='Code',)

