from flectra import api, fields, models, _

class MarelInMrpProduction(models.Model):
    """ Manufacturing Orders """
    #_name = 'mrp.production'
    #_description = 'Manufacturing Order'
    #_date_name = 'date_planned_start'
    _inherit = ['mrp.production']
    #_order = 'date_planned_start asc,id'
    
    #relasi_tambahan = fields.Many2one('mrp.production', string="Relasi Tambahan", required=True)

    halaman_report = fields.Integer(string='Halaman Report',store=True, compute="change_halaman_report_",)
    #compute="change_halaman_report_",
    
    #@api.onchange('product_qty')
    #def change_halaman_report(self):
        #self.halaman_report = self.product_qty
    
    @api.depends('product_qty','halaman_report')
    def change_halaman_report_ (self):
        self.halaman_report = self.product_qty/48
    
    #@api.depends('product_qty','halaman_report')
    #def change_halaman_report_ (self):
        #if self.product_qty:
            #self.halaman_report = self.product_qty
    