from flectra import api, fields, models, _
from flectra.addons import decimal_precision as dp
from flectra.exceptions import UserError
from flectra.tools import float_is_zero
import math

class in_mrp_product_produce(models.TransientModel):
    _name = 'inmrp.product.produce'
    #_inherits = {'mrp.product.produce': 'display_name','mrp.production':'name'}
    _inherit = ['mrp.product.produce']


    @api.model
    def run_sql(self, qry):
    self._cr.execute(qry)
    _res = self._cr.fetchall()
    return _res

