# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

{
    'name': 'Marel In Report MO',
    'author': 'Odoo S.A.',
    'version': '1.2',
    'category': 'Sales',
    'depends': ['base', 'mrp'],
    'description': """
This is the base module for managing products and pricelists in Odoo, Flectra.
==============================================================================

Products support variants, different pricing methods, vendors information,
make to stock/order, different units of measure, packaging and properties.

Pricelists support:
-------------------
    * Multiple-level of discount (by product, category, quantities)
    * Compute price based on different criteria:
        * Other pricelist
        * Cost price
        * List price
        * Vendor price

Pricelists preferences by product and/or partners.

Print product labels with barcode.
    """,
    'data': [
        'report/marel_report_kkp.xml',
        'report/report_marel_in_report_mo_doc.xml',
        # ------------------
        'report/marel_report_kkp_sisa.xml',
        'report/report_marel_in_report_mo_sisa_doc.xml',
    ],
    'demo': [
    ],
	"installable": True,
	"auto_install": False,
    "application": True,
}
